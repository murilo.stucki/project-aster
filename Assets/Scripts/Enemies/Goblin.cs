using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : Enemy
{
    [SerializeField] List<AttackStance> stances;
    
    [SerializeField] Transform attackPoint;
    [SerializeField] LayerMask enemyLayers;


    protected override void Attack()
    {
        int randomStance = UnityEngine.Random.Range(0, stances.Count);
        animator.SetTrigger("t_attack" + randomStance);
        AttackStance stance = stances[randomStance];
        StartCoroutine(AnimationLock(stance.animationLockTime));
    }

    public override void HitDetection(int stanceIndex)
    {
        AttackStance stance = stances[stanceIndex];
        if (attackPoint != null)
        {
            Collider2D[] enemiesHit = Physics2D.OverlapCircleAll(attackPoint.position, stance.range, enemyLayers);

            foreach (Collider2D enemy in enemiesHit)
            {
                // If its another Enemy but not itself or if its a Player
                if (enemy.gameObject.layer == gameObject.layer && !enemy.gameObject.Equals(gameObject))
                    enemy.GetComponent<Enemy>().TakeDamage(stance.damage);
                else if (enemy.gameObject.layer == player.layer)
                    playerCombat.TakeDamage(stance.damage, true);
            }
        }
        else
        {
            Debug.Log("No attack point defined!");
        }
    }

    public void MoveHorizontal(float velocityX)
    {
        if (isFacingRight)
		{
			rigidBody.velocity = new Vector2(-velocityX, 0);
		}
		else
		{
			rigidBody.velocity = new Vector2(velocityX, 0);
		}
    }
}
