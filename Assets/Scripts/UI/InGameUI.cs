using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    public static InGameUI SharedInstance;
    [SerializeField] Image healthBarFill;
    [SerializeField] GameObject gameOverScreen;
    //[SerializeField] GameObject playerObject;

    private void Awake() 
    {
        if (SharedInstance == null)
        {
            SharedInstance = this;
        }

        if (healthBarFill == null)
        {
            healthBarFill = transform.Find("Health Bar").Find("Fill").GetComponent<Image>();
        }
        if (gameOverScreen == null)
        {
            gameOverScreen = transform.Find("Game Over").gameObject;
        }
    }

    private void Start() 
    {
        gameOverScreen.SetActive(false);
    }

    public void UpdateHealthBar(float fillAmount)
    {
        healthBarFill.fillAmount = fillAmount;
    }

    public void SetGameOverScreenActive()
    {
        gameOverScreen.SetActive(true);
    }
    public void SetGameOverScreenActive(bool active)
    {
        gameOverScreen.SetActive(active);
    }
}
