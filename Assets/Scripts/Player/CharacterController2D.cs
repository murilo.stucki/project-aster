using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CharacterController2D : MonoBehaviour
{
	public static PlayerInputActions playerInput;
	[SerializeField] private float m_JumpForce = 400f;							// Amount of force added when the player jumps.
	[SerializeField] private float m_DashSpeed = 2f;							// Speed applied when the player dashes.
	[SerializeField] private float m_DashCooldown = 1.0f;						// Minimum time between dashes.
	private float m_TimeToDash = 0.0f;
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;			// Amount of maxSpeed applied to crouching movement. 1 = 100%
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
	[SerializeField] private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
	[SerializeField] private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
	[SerializeField] private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.
	[SerializeField] private Transform m_CeilingCheck;							// A position marking where to check for ceilings
	[SerializeField] private Collider2D m_CrouchDisableCollider;				// A collider that will be disabled when crouching
	[SerializeField] private float flipPositionOffset;
	[SerializeField] Animator animator;
	[SerializeField] float bottomYBound;
	[SerializeField] const float k_GravityScale = 3.0f;
    [SerializeField] Image staminaBarFill;

	const float k_GroundedRadius = .1f; // Radius of the overlap circle to determine if grounded
	public bool m_Grounded {get; private set;}            // Whether or not the player is grounded.
	public bool isRunning;			// Whether or not the player is running.
	public bool isDashing;			// Whether or not the player is dashing.
	public bool isSliding;			// Whether or not the player is sliding.
	public bool isAttacking; 			// Whether or not the player is attacking so it can't move.
	public bool isDead;				// Whether or not the player is dead.
	const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;  // For determining which way the player is currently facing.
	public int directionFacing {get; private set;}
	private Vector2 m_Velocity = Vector2.zero;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;
	public UnityEvent OnDashEvent;

	public UnityEvent OnDeathEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	public BoolEvent OnCrouchEvent;
	public bool m_wasCrouching {get; private set;}

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();
		playerInput = new PlayerInputActions();
		playerInput.PlayerDefault.Enable();

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();
		
		if (animator == null)
		{
			animator = GetComponent<Animator>();
		}

		if (staminaBarFill == null)
        {
            staminaBarFill = transform.Find("Canvas").Find("Stamina Bar").Find("Fill").GetComponent<Image>();
        }
	}

	private void Start() 
	{
		isRunning = false;
		isDashing = false;
		isAttacking = false;
		isSliding = false;
		m_wasCrouching = false;		
		isDead = false;
		m_Rigidbody2D.gravityScale = k_GravityScale;
		UpdateStaminaBar(1.0f);
	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = CheckGround();
		if (!wasGrounded && m_Grounded)
		{
			OnLandEvent.Invoke();
			if (m_Rigidbody2D.velocity.y > 0)
				SetVerticalVelocity(0);
		}
	}

	private void LateUpdate() 
	{
		if(m_FacingRight)
			directionFacing = 1;
		else
			directionFacing = -1;
	}

	private void Update()
	{
		if (transform.position.y <= bottomYBound) //Resets position when the player fall.
		{
			Die();
		}
		
		// Decrement dash cooldown
		if (m_TimeToDash > 0)
		{
			m_TimeToDash -= Time.deltaTime;
			float fillAmount = 1.0f - (m_TimeToDash / m_DashCooldown);
			UpdateStaminaBar(Mathf.Min(1.0f, fillAmount));
		}
	}


	public void Move(float move, bool crouch, bool jump, bool dash)
	{
		//Vector2 zeroYVelocity = new Vector2(m_Rigidbody2D.velocity.x, 0.0f);

		//If the player is Dead, hard stop
		if (isDead)
		{
			SetHorizontalVelocity(0);
			return;
		}

		// If the player is current in an attack routine, smoothly stop its horizontal movement
		if (isAttacking)
		{
			Vector2 zeroXVelocity = new Vector2(0, m_Rigidbody2D.velocity.y);
			m_Rigidbody2D.velocity = Vector2.SmoothDamp(m_Rigidbody2D.velocity, zeroXVelocity, ref m_Velocity, 0.2f);
			isDashing = false;
			isRunning = false;
			return;
		}

		// If the player is dashing it can't move in the vertical axis 
		if (isDashing)
		{
			SetVerticalVelocity(0);
		}

		// If crouching, check to see if the character can stand up
		if (!crouch)
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
				dash = false; // Can't dash in tight places
			}
		}

		//only control the player if grounded or airControl is turned on
		if (m_Grounded || m_AirControl)
		{

			// If crouching
			if (crouch)
			{
				if (!isRunning)
				{
					// Reduce the speed by the crouchSpeed multiplier
					move *= m_CrouchSpeed;
				} 
				else if(!isSliding)
				{
					animator.SetTrigger("t_slide");
					StartCoroutine(Slide());
				}

				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					OnCrouchEvent.Invoke(true);
				}

				// Disable one of the colliders when crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = false;
			} else
			{
				// Enable the collider when not crouching
				if (m_CrouchDisableCollider != null)
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					OnCrouchEvent.Invoke(false);
				}
			}

			// Move the character by finding the target velocity
			Vector2 targetVelocity = new Vector2(move, m_Rigidbody2D.velocity.y);
			// And then smoothing it out and applying it to the character
			m_Rigidbody2D.velocity = Vector2.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			// If the input is moving the player right and the player is facing left...
			if (move > 0 && !m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			else if (move < 0 && m_FacingRight)
			{
				// ... flip the player.
				Flip();
			}

			if (dash && m_TimeToDash <= 0.0f && !isSliding)
			{
				OnDashEvent.Invoke();
				StartCoroutine(Dash());
			}
		}

		// If the player should jump...
		if (m_Grounded && jump)
		{
			isDashing = false;
			isSliding = false;
			isRunning = false;
			// Add a vertical force to the player.
			SetVerticalVelocity(0);
			m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce), ForceMode2D.Impulse);
			
		}

        animator.SetFloat("f_speedx", Mathf.Abs(m_Rigidbody2D.velocity.x));
        animator.SetFloat("f_speedy", m_Rigidbody2D.velocity.y);
		animator.SetBool("b_grounded", m_Grounded);
		animator.SetBool("b_crouch", crouch);
		animator.SetBool("b_dashing", isDashing);
		animator.SetBool("b_running", isRunning);
	}

	IEnumerator Dash()
	{
		isDashing = true;
		m_TimeToDash = m_DashCooldown;
		UpdateStaminaBar(0.0f);
		m_MovementSmoothing *= 2f;
		float velocityX = m_DashSpeed * transform.localScale.x;

		SetVelocity(velocityX, 0);

		yield return new WaitForSeconds(0.3f);

		m_MovementSmoothing /= 2f;
		if(isDashing) //if the dash wasn't interrupted
		{
			SetHorizontalVelocity(velocityX * 0.1f);
			isDashing = false;
		}
	}

	IEnumerator Slide()
	{
		isSliding = true;
		isDashing = false;
		m_MovementSmoothing *= 2f;

		yield return new WaitForSeconds(0.4f);

		m_MovementSmoothing /= 2f;
		if(isSliding) //if the slide wasn't interrupted
		{
			SetHorizontalVelocity(m_Rigidbody2D.velocity.x * m_CrouchSpeed);
			isSliding = false;
		}
	}

	private void DisableGravity()
	{
		m_Rigidbody2D.gravityScale = 0;
	}

	private void EnableGravity()
	{
		m_Rigidbody2D.gravityScale = k_GravityScale;
	}

	private void Flip()
	{
		if (m_FacingRight)
		{
			transform.position += Vector3.left * flipPositionOffset;
		}
		else
		{
			transform.position += Vector3.right * flipPositionOffset;
		}
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void SetVelocity(float velocityX, float velocityY)
	{
		m_Rigidbody2D.velocity = new Vector2(velocityX, velocityY);
	}

	public void SetHorizontalVelocity(float velocity)
	{
		m_Rigidbody2D.velocity = new Vector2(velocity, m_Rigidbody2D.velocity.y);
	}

	public void SetVerticalVelocity(float velocity)
	{
		m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, velocity);
	}
	
	private bool CheckGround()
	{
		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		if (colliders.Length > 0)
		{
			for (int i = 0; i < colliders.Length; i++)
			{
				if (colliders[i].gameObject != gameObject)
				{
					return true;
				}
			}
		}
		return false;
	}

	public void Die()
	{
		isDead = true;
		isAttacking = false;
		isDashing = false;
		isRunning = false;
		isSliding = false;
		OnDeathEvent.Invoke();
	}

	public void UpdateStaminaBar(float fillAmount)
    {
        staminaBarFill.fillAmount = fillAmount;
        if (fillAmount == 1.0f)
        {
            staminaBarFill.transform.parent.gameObject.SetActive(false);
        }
        else if (!staminaBarFill.transform.parent.gameObject.activeInHierarchy)
        {
            staminaBarFill.transform.parent.gameObject.SetActive(true);
        }
    }
}
