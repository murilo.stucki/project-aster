using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public void RestartGameScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void GameOver()
    {
        InGameUI.SharedInstance.SetGameOverScreenActive();
    }
}
